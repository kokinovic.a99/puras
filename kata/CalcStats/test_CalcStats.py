"""Test modul for CalcStats class"""

import unittest

from CalcStats import CalcStats


class TestMinimumValue(unittest.TestCase):
    """Testing Scenario_1"""

    def setUp(self):
        self.my_CalcStats = CalcStats()

    def test_should_return_correct_minimum_value_of_positive_numbers(self):
        array = [1, 2, 3, 4, 5]
        result = self.my_CalcStats.minimum(array)
        self.assertEqual(1, result)

    def test_should_return_correct_minimum_value_of_negative_numbers(self):
        array = [-1, -2, -3, -4, -5]
        result = self.my_CalcStats.minimum(array)
        self.assertEqual(-5, result)

    def test_should_return_correct_minimum_value_of_any_numbers(self):
        array = [1, -2, 3, -4, 5]
        result = self.my_CalcStats.minimum(array)
        self.assertEqual(-4, result)

class TestMaximumValue(unittest.TestCase):
    """Testing Scenario_2"""

    def setUp(self):
        self.my_CalcStats = CalcStats()

    def test_should_return_correct_maximum_value_of_positive_numbers(self):
        array = [1, 2, 3, 4, 5]
        result = self.my_CalcStats.maximum(array)
        self.assertEqual(5, result)

    def test_should_return_correct_maximum_value_of_negative_numbers(self):
        array = [-1, -2, -3, -4, -5]
        result = self.my_CalcStats.maximum(array)
        self.assertEqual(-1, result)

    def test_should_return_correct_maximum_value_of_any_numbers(self):
        array = [1, -2, 3, -4, 5]
        result = self.my_CalcStats.maximum(array)
        self.assertEqual(5, result)

class TestNumberOfElementsValue(unittest.TestCase):
    """Testing Scenario_4"""

    def setUp(self):
        self.my_CalcStats = CalcStats()

    def test_should_return_correct_number_of_elements_value_of_positive_numbers(self):
        array = [1, 2, 3, 4, 5]
        result = self.my_CalcStats.number_of_elements(array)
        self.assertEqual(5, result)

    def test_should_return_correct_number_of_elements_value_of_negative_numbers(self):
        array = [-1, -2, -3, -4, -5, -6, -7]
        result = self.my_CalcStats.number_of_elements(array)
        self.assertEqual(7, result)

    def test_should_return_correct_number_of_elements_value_of_any_numbers(self):
        array = [1, -2, 3, -4, 5, 6, -7, 8]
        result = self.my_CalcStats.number_of_elements(array)
        self.assertEqual(8, result)

class TestAverageValue(unittest.TestCase):
    """Testing Scenario_3"""

    def setUp(self):
        self.my_CalcStats = CalcStats()

    def test_should_return_correct_average_value_of_positive_numbers(self):
        array = [1, 2, 3, 4, 5]
        result = self.my_CalcStats.average(array)
        self.assertEqual(3, result)

    def test_should_return_correct_average_value_of_negative_numbers(self):
        array = [-1, -2, -3, -4, -5]
        result = self.my_CalcStats.average(array)
        self.assertEqual(-3, result)

    def test_should_return_correct_average_value_of_any_numbers(self):
        array = [1, -2, 3, -4, 5, -6]
        result = self.my_CalcStats.average(array)
        self.assertEqual(-0.5, result)

unittest.main()
