# Feature: Calc Stats

## Narrative

**As a** customer
**I want** to process sequence of integer numbers
**So that** I can determine average, minimum and maximum values, as well as number of elements in sequence

## Scenario_1:

**Given**: Sequence of integer numbers <br>
**When**: Processing is performed <br>
**Then**: Correct minimum value is returned <br>

## Scenario_2:

**Given**: Sequence of integer numbers <br>
**When**: Processing is performed <br>
**Then**: Correct maximum value is returned <br>

## Scenario_3:

**Given**: Sequence of integer numbers <br>
**When**: Processing is performed <br>
**Then**: Correct average value is returned <br>

## Scenario_4:

**Given**: Sequence of integer numbers <br>
**When**: Processing is performed <br>
**Then**: Correct number of elements is returned <br>
