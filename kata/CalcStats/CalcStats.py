"""CalcStats modul"""


class CalcStats:
    """CalcStats class"""

    def minimum(self, arr):
        min = arr[0]

        for i in arr:
            if(i < min):
                min = i

        return min

    def maximum(self, arr):
        max = arr[0]

        for i in arr:
            if(i > max):
                max = i

        return max

    def number_of_elements(self, arr):
        num_of_elements = len(arr)

        return num_of_elements

    def average(self, arr):
        avg = 0

        for i in arr:
            avg += i

        return avg/self.number_of_elements(arr)
